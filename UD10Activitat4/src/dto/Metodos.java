package dto;
import View.view;
import excepciones.custom;
import excepciones.exepciones;

public class Metodos {
	
	//Aqui empiezo el programa en si
	public static void calculadora() throws custom {
		String operacion = view.preguntarOperacion();
		double num1;
		double num2;

		double resultado = 0;
		
		switch (operacion) {
		case "suma":
			num1 = exepciones.tryPregunarNumero();
			num2 =  exepciones.tryPregunarNumero();
			resultado = suma(num1, num2);
			break;
		case "resta":
			num1 = exepciones.tryPregunarNumero();
			num2 =  exepciones.tryPregunarNumero();
			resultado = resta(num1, num2);
			break;
		case "division":
			try {
				num1 = exepciones.tryPregunarNumero();
				num2 =  exepciones.tryPregunarNumero();
				if (num1 == 0 || num2 == 0) {
					throw new custom(2);
				}
				resultado = division(num1, num2);
			}catch(custom ej) {
				System.out.println(ej.getMessage());
			}
			break;
		case "multiplicacion":
			num1 = exepciones.tryPregunarNumero();
			num2 =  exepciones.tryPregunarNumero();
			resultado = multiplicacion(num1, num2);
			break;
		case "potencia":
			num1 = exepciones.tryPregunarNumero();
			num2 =  exepciones.tryPregunarNumero();
			resultado = potencia(num1, num2);
			break;
		case "raizCuadrada":
			try {
				num1 = exepciones.tryPregunarNumero();
				if (num1 < 0) {
					throw new custom(1);
				}
				resultado = raizCuadrada(num1);
			}catch(custom ej) {
				System.out.println(ej.getMessage());
			}
			break;
		case "raizCubica":
			try {
				num1 = exepciones.tryPregunarNumero();
				if (num1 < 0) {
					throw new custom(0);
				}
				resultado = raizCubica(num1);
			}catch(custom ej) {
				System.out.println(ej.getMessage());
			}
		}
		System.out.println(resultado);
	}
	
	//Metodo de la suma
	public static double suma(double num1, double num2) {
		return num1 + num2;
	}
	//Metodo de la resta
	public static double resta(double num1, double num2) {
		return num1 - num2;	
	}
	//Metodo de la multiplicación
	public static double multiplicacion(double num1, double num2) {
		return num1 * num2;
	}
	//Metodo de la división
	public static double division(double num1, double num2) {
		return num1 / num2;
	}
	//Metodo de la potencia
	public static double potencia(double num1, double num2) {
		return Math.pow(num1, num2);
	}
	//Metodo de la raiz cuadrada
	public static double raizCuadrada(double num1) {
		return Math.sqrt(num1);
	}
	//Metodo de la raiz cubica
	public static double raizCubica(double num1) {
		return Math.cbrt(num1);
	}
}
