package View;
import javax.swing.JOptionPane;

public class view {
	//Aqui pregunto que operacion quiere hacer
	public static String preguntarOperacion() {
		String operacion;
		do {
			operacion = JOptionPane.showInputDialog(null, "Que operacion quieres hacer(suma, resta, multiplicacion, division, potencia, raizCuadrada, raizCubica)");
		} while (operacion.equals("suma") && operacion.equals("resta") && operacion.equals("multiplicacion") && operacion.equals("division") &&
				operacion.equals("potencia") && operacion.equals("raizCuadrada") && operacion.equals("raizCubica"));
		
		return operacion;
	}
	//Aqui pregunto un n�mero
	public static double preguntarNumero() {
		String operacion = JOptionPane.showInputDialog(null, "Pon un numero");
		return Double.parseDouble(operacion);
	}
}
