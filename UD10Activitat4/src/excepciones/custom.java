package excepciones;

public class custom extends Exception{
	private int codigoError;
	//Primero hago el constructor por defecto y al que le pasas el codigo de error
	public custom() {
		super();
	}
	
	public custom(int num) {
		super();
		this.codigoError = num;
	}
	
	public String getMessage() {
		String mensage = " ";
		
		switch (codigoError) {
		case 2:
			mensage = "No se puede poner un 0 en una division";
			break;
		case 1:
			mensage = "No se puede poner un numero negativo en una raiz Cuadrada";
			break;
		case 0:
			mensage = "No se puede poner un numero negativo en una raiz Cubica";
			break;
		}
		
		return mensage;
	}
	
	
}
