package excepciones;

import View.view;

public class exepciones {
	//Aqui tengo un try catch para verificar que ponga un n�mero
	public static double tryPregunarNumero() {
		double num = 0;
		boolean numValido = false;
		
		do {
			try {
				num = view.preguntarNumero();
				
				numValido = true;
			}catch (NumberFormatException a) {
				System.out.println("No has puesto un n�mero valido");
			}
			return num;
			
		}while(numValido);
	}
}
	